import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        value: 'Not authorized',
    },
    reducers: {
        login: (state) => {
            state.value = 'Authorized'
        },
        logout: (state) => {
            state.value = 'Not authorized'
        }
    }
})

export const { login, logout } = authSlice.actions;

export default authSlice.reducer;