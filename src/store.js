import { configureStore } from "@reduxjs/toolkit";
import authReducer from './authSlice';

const store = configureStore({
    reducer: {
        authorization: authReducer,
    },    
});

export default store;
