import { useDispatch } from "react-redux";
import { login, logout} from "../authSlice";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import '../style.css';


export default function Login(){
        
    const dispatch = useDispatch();
    
    const loginButtonHandler = () => {
        dispatch(login());
    }
    
    const logoutButtonHandler = () => {
        dispatch(logout());
    }
    
    return(               
        <div>
            <div>
            <label>Username</label>
            <p><input type="text" placeholder="Input your name..."></input></p>
            </div>
            <div>
            <label>Password</label>
            <p><input type="password" placeholder="Input your password..."></input></p>
            </div>
            <div>
            <span><Button variant='primary' onClick={() => dispatch(login())}>Login</Button></span>
            <span><Button onClick={() => dispatch(logout())}>Logout</Button></span>
            </div>            
        </div>        
    )
};