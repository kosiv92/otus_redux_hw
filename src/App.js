import Homepage from './components/Homepage';
import Login from './components/Login';
import Register from './components/Register';
import Error from './components/Error';
import { useDispatch, useSelector } from "react-redux";
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";

function App() {
      const isAuthorize = useSelector(state => (state.authorization.value));

  return (
    <div>
    <div>Is Authorize: {isAuthorize}</div> 
<BrowserRouter>    
    <nav>
      <ul>        
        <li>
          <Link to={'/login'}>Login</Link>
        </li>
        <li>
          <Link to={'/register'}>Register</Link>
        </li>        
      </ul>
    </nav>

    <Routes>
      <Route index element={<Homepage/>}/>
      <Route path="login" element={<Login/>}/>
      <Route path="register" element={<Register/>}/>      
      <Route path="*" element={<Error/>}/>
    </Routes>
    </BrowserRouter>
    </div>
    
    
  );
}

export default App;
